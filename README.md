# **Crypted**
[![Author](https://img.shields.io/badge/author-Kuroyasha512-blue.svg)](https://gitlab.com/Kuroyasha512)
[![Software License](https://img.shields.io/badge/license-GNU_GPLv3-brightgreen.svg)](https://gitlab.com/Kuroyasha512/crypted/blob/master/COPYING)
[![Current](https://img.shields.io/badge/current-V1.7---.svg)](https://gitlab.com/Kuroyasha512/crypted/releases)
[![os](https://img.shields.io/badge/os-GNU/Linux-red.svg)](https://www.gnu.org/gnu/linux-and-gnu.en.html)

Crypted - Cryptograpy in terminal

## Setup
```sh
git clone https://gitlab.com/batuesh/crypted.git 
cd crypted
sudo chmod +x Release.sh
./Release.sh
cd Build/Release
./Crypted -h
#see doc for more options
```
## License

This project is licensed under [![LICENSE](https://www.gnu.org/graphics/gplv3-with-text-84x42.png)](https://gitlab.com/Kuroyasha512/crypted/blob/master/COPYING) - see the [LICENSE](https://gitlab.com/Kuroyasha512/crypted/blob/master/COPYING) file for details