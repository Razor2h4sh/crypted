/**************************************************************************
 *   railfence.h  --  This file is part of Crypted.                       *
 *                                                                        *
 *   Copyright (C) 2019 batuesh                                           *
 *                                                                        *
 *   Crypted is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   Crypted is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#ifndef _RAILFENCE_H
#define _RAILFENCE_H

#ifndef _GLIBCXX_IOSTREAM
#include <iostream>
#endif
#ifndef _GLIBCXX_VECTOR
#include <vector>
#endif

class fence
{
    public:
        fence(std::basic_string<char> &input, int &rail);
        void encrypt();
        void decrypt();
        std::basic_string<char> get_input() const;
    private:
        int rail = 0;
        std::basic_string<char> input = "", temp = "";
};

#endif /* _RAILFENCE_H */
