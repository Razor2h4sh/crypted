/**************************************************************************
 *   manager.h  --  This file is part of Crypted.                         *
 *                                                                        *
 *   Copyright (C) 2019 batuesh                                           *
 *                                                                        *
 *   Crypted is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   Crypted is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#ifndef _MANAGER_H
#define _MANAGER_H

#ifndef _GLIBCXX_IOSTREAM
#include <iostream>
#endif

#ifndef _GLIBCXX_VECTOR
#include <vector>
#endif

#ifndef _GLIBCXX_STRING
#include <string>
#endif

#ifndef _GLIBCXX_LOCALE
#include <locale>
#endif

#ifndef _GLIBCXX_ARRAY
#include <array>
#endif

#ifndef _GETOPT_H
#include <getopt.h>
#endif

#ifndef _GLIBCXX_CSTDLIB
#include <cstdlib>
#endif

#ifndef _GLIBCXX_CSTDIO
#include <cstdio>
#endif

#ifndef _GLIBCXX_CCTYPE
#include <cctype>
#endif

#ifndef _SCYTALE_H
#include "scytale.h"
#endif

#ifndef _ATBASH_H
#include "atbash.h"
#endif

#ifndef _CAESAR_H
#include "caesar.h"
#endif

#ifndef _AFFINE_H
#include "affine.h"
#endif

#ifndef _RAILFENCE_H
#include "railfence.h"
#endif

#ifndef _KEYWORD_H
#include "keyword.h"
#endif

#endif /* MANAGER_H */
