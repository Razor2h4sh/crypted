/**************************************************************************
 *   keyword.cpp  --  This file is part of Crypted.                       *
 *                                                                        *
 *   Copyright (C) 2019 batuesh                                           *
 *                                                                        *
 *   Crypted is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   Crypted is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#include "keyword.h"

keyword::keyword(std::basic_string<char> &input, std::basic_string<char> &key)
{
    this->input = input;
    this->temp = input;
    this->key = key;
}

void keyword::strip_double_char(std::basic_string<char> &input)
{
    for (size_t i = 0;i < input.size();++i)
        for (size_t x = (i+1);x < input.size();++x)
            if (input[i] == input[x])
                for (size_t y = i;y < input.size();++y)
                    input[x] = input[x+1];
}

void keyword::abcrypt()
{
    std::basic_string<char> &lower_key = this->key;
    std::basic_string<char> &upper_encrypting = this->upper_encrypting;
    std::basic_string<char> &lower_encrypting = this->lower_encrypting;
    std::basic_string<char> upper_key = "";
    int &base_upper_code = this->base_upper_code;
    int &base_lower_code = this->base_lower_code;
    for (size_t i = 0;i < lower_key.size();++i)
        if (islower(lower_key[i]))
            upper_key.push_back(toupper(lower_key[i]));
    for (size_t i = 0;i < ALPHABET;++i)
        upper_encrypting.push_back(static_cast<char>(base_upper_code + i));
    for (size_t i = 0;i < ALPHABET;++i)
        lower_encrypting.push_back(static_cast<char>(base_lower_code + i));
    strip_double_char(key);

    for (size_t i = 0;i < upper_encrypting.size();++i)
        for (size_t x = 0;x < upper_key.size();++x)
            if (upper_encrypting[i] == upper_key[x])
                for (size_t y = i;y < upper_encrypting.size();++y)
                    upper_encrypting[y] = upper_encrypting[y+1];
    upper_encrypting.insert(0, upper_key);
    for (size_t i = 0;i < upper_encrypting.size();++i)
        if (upper_encrypting[i] == '\0')
            for (size_t x = i;x < upper_encrypting.size();++x)
                upper_encrypting[x] = upper_encrypting[x+1];
    strip_double_char(upper_encrypting);

    for (size_t i = 0;i < lower_encrypting.size();++i)
        for (size_t x = 0; x < lower_key.size(); ++x)
            if (lower_encrypting[i] == lower_key[x])
                for (size_t y = i;y < lower_encrypting.size();++y)
                    lower_encrypting[y] = lower_encrypting[y+1];
    lower_encrypting.insert(0, lower_key);
    for (size_t i = 0;i < lower_encrypting.size();++i)
        if (lower_encrypting[i] == '\0')
            for (size_t x = i;x < lower_encrypting.size();++x)
                lower_encrypting[x] = lower_encrypting[x+1];
    strip_double_char(lower_encrypting);
}

void keyword::abcplain()
{
    std::basic_string<char> &upper_plain = this->upper_plain;
    std::basic_string<char> &lower_plain = this->lower_plain;
    int &base_upper_code = this->base_upper_code;
    int &base_lower_code = this->base_lower_code;
    for (size_t i = 0;i < ALPHABET;++i)
        upper_plain.push_back(static_cast<char>(base_upper_code + i));
    for (size_t i = 0;i < ALPHABET;++i)
        lower_plain.push_back(static_cast<char>(base_lower_code + i));
}

void keyword::encrypt()
{
    std::basic_string<char> &input = this->input;
    std::basic_string<char> &temp = this->temp;
    std::basic_string<char> &upper_encrypting = this->upper_encrypting;
    std::basic_string<char> &lower_encrypting = this->lower_encrypting;
    int &code = this->code;
    for (size_t i = 0;i < input.size();++i)
    {
        if ( isupper(input[i]) )
            code = 65;
        else if ( islower(input[i]) )
            code = 97;
        else
            continue;
        temp[i] = temp[i] - code;
    }
    for (size_t i = 0;i < input.size();++i)
        if ( isupper(input[i]) )
            input[i] = upper_encrypting[temp[i]];
        else if ( islower(input[i]) )
            input[i] = lower_encrypting[temp[i]];
        else
            continue;
}

void keyword::decrypt()
{
    std::basic_string<char> &input = this->input;
    std::basic_string<char> &temp = this->temp;
    std::basic_string<char> &upper_plain = this->upper_plain;
    std::basic_string<char> &lower_plain = this->lower_plain;
    std::basic_string<char> &upper_encrypting = this->upper_encrypting;
    std::basic_string<char> &lower_encrypting = this->lower_encrypting;
    for (size_t i = 0;i < input.size();++i)
        if ( isupper(input[i]) )
            input[i] = upper_plain[upper_encrypting.find(temp[i])];
        else if ( islower(input[i]) )
            input[i] = lower_plain[lower_encrypting.find(temp[i])];
        else
            continue;
}

std::basic_string<char>
keyword::get_input() const
{
    return this->input;
}
